#!/usr/bin/env bash
# @brief;
# runs the demo navigation on the rosbot

# getting the current directory
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"

# sending the config to the rosbot
source $DIR/send_config_to_rosbot.sh

gnome-terminal -- ssh husarion@husarion.local "bash -c \"source /opt/ros/kinetic/setup.bash;
source ~/.bashrc;
export ROS_MASTER_URI=http://husarion.local:11311;
export ROS_IP=husarion.local;
roslaunch ~/mowito/launch/rosbot/run_rosbot_mapping_with_explore.launch;
exec bash\""