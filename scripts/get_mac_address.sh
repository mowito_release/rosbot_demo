#!/usr/bin/env bash
# @brief;
# gets the mac addresses of the rosbot and saves it in mac_address.txt in home directory

ssh husarion@husarion.local 'cat /sys/class/net/*/address' >> ~/mac_address.txt
