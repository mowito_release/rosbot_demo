#!/usr/bin/env bash
# @brief;
# runs the manual control

# gnome-terminal -e "bash -c \"source /opt/ros/melodic/setup.bash; 
# export ROS_MASTER_URI=http://husarion.local:11311;
# source ~/rosbot_demo/scripts/ros_ip.sh;
# rosrun teleop_twist_keyboard teleop_twist_keyboard.py;
# exec bash\""

gnome-terminal -- ssh husarion@husarion.local "bash -c \"source /opt/ros/kinetic/setup.bash;
source ~/.bashrc;
export ROS_MASTER_URI=http://husarion.local:11311;
export ROS_IP=husarion.local;
rosrun teleop_twist_keyboard teleop_twist_keyboard.py;
exec bash\""