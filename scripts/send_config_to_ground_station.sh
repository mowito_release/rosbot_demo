#!/usr/bin/env bash
# @brief;
# sets config in the ground station from rosbot

ros_version=$1
if [ -z "$1" ]
  then
    echo "defaulting to kinetic"
    ros_version=kinetic
fi
# delete the current config
rm -rf ~/rosbot_demo/config
scp -r  husarion@husarion.local:~/mowito/config ~/rosbot_demo/