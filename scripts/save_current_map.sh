#!/usr/bin/env bash
# @brief;
# runs the manual control

gnome-terminal -- ssh husarion@husarion.local "bash -c \"source /opt/ros/kinetic/setup.bash;
source ~/.bashrc;
export ROS_MASTER_URI=http://husarion.local:11311;
export ROS_IP=husarion.local;
rosrun map_server map_saver -f mymap\""