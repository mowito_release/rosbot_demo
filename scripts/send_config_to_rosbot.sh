#!/usr/bin/env bash
# @brief;
# sets config in the rosbot based on ground station

ros_version=$1
if [ -z "$1" ]
  then
    echo "defaulting to kinetic"
    ros_version=kinetic
fi
# delete the current config
ssh husarion@husarion.local 'rm -rf ~/mowito/config'
scp -r ~/rosbot_demo/config husarion@husarion.local:~/mowito/