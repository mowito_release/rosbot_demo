#!/usr/bin/env bash
# @brief;
# runs rviz on the ground station running melodic

# getting the current directory
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"

# sending the config to the rosbot
source $DIR/send_config_to_rosbot.sh

gnome-terminal -e "bash -c \"source /opt/ros/melodic/setup.bash; 
export ROS_MASTER_URI=http://husarion.local:11311;
source $DIR/ros_ip.sh;
rviz -d $DIR/default.rviz\""