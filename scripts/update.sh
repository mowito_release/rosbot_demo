#!/usr/bin/env bash
# @brief;
# uninstalls the previous version and installs the latest version

echo "pulling rosbot demo"
git  -C ~/rosbot_demo reset --hard;
git -C ~/rosbot_demo pull; 
echo "now removing and setting up new changes in rosbot"
gnome-terminal -- ssh husarion@husarion.local '~/mowito/remove_mowito.sh kinetic; 
git -C ~/mowito reset --hard;
git -C ~/mowito pull; 
git -C ~/mowito checkout ros_kinetic_release_20.03_arm_v7l; 
~/mowito/setup_mowito.sh kinetic;
echo "---------------update complete----------------";
bash'