#!/usr/bin/env bash
# @brief;
# stops the exploration using the service call

gnome-terminal -- ssh husarion@husarion.local "bash -c \"source /opt/ros/kinetic/setup.bash;
source ~/.bashrc;
export ROS_MASTER_URI=http://husarion.local:11311;
export ROS_IP=husarion.local;
rosservice call /explore/stop_exploration "{}"\""