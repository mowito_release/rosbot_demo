#!/usr/bin/env bash
# @brief;
# for aborting the mission

gnome-terminal -- ssh husarion@husarion.local "bash -c \"source /opt/ros/kinetic/setup.bash;
source ~/.bashrc;
export ROS_MASTER_URI=http://husarion.local:11311;
export ROS_IP=husarion.local;
rosservice call /mission_executive/abort_mission "{}"\""



 
