#!/usr/bin/env bash
# @brief;
# runs the demo navigation on the rosbot

export ROS_IP=$(ip addr | grep -Eo 'inet (addr:)?([0-9]*\.){3}[0-9]*' | grep -Eo '([0-9]*\.){3}[0-9]*' | grep -v '127.0.0.1')