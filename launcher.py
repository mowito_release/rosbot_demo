#!/usr/bin/env python3

try:
    from tkinter import *
except:
    from Tkinter import *

from PIL import Image, ImageTk
import subprocess
import glob
import math


class ToolTip(object):

    def __init__(self, widget, text):
        self.waittime = 500
        self.wraplength = 180
        self.widget = widget
        self.text = text
        self.widget.bind("<Enter>", self.enter)
        self.widget.bind("<Leave>", self.leave)
        self.widget.bind("<ButtonPress>", self.leave)
        self.idx = None
        self.tw = None

    def enter(self, event=None):
        self.schedule()

    def leave(self, event=None):
        self.unschedule()
        self.hidetip()

    def schedule(self):
        self.unschedule()
        self.idx = self.widget.after(self.waittime, self.showtip)

    def unschedule(self):
        idx = self.idx
        self.idx = None
        if idx:
            self.widget.after_cancel(idx)

    def showtip(self, event=None):
        x, y, cx, cy = self.widget.bbox("insert")
        x += self.widget.winfo_rootx() + 25
        y += self.widget.winfo_rooty() + 20
        self.tw = Toplevel(self.widget)
        self.tw.wm_overrideredirect(True)
        self.tw.wm_geometry("+%d+%d" % (x, y))
        label = Label(self.tw, text=self.text, justify='left',
                      background="#ffffff", relief='solid', borderwidth=1,
                      wraplength=self.wraplength)
        label.pack(ipadx=1)

    def hidetip(self):
        tw = self.tw
        self.tw = None
        if tw:
            tw.destroy()


class LabelImage(Label):

    def __init__(self, master, tool_tip=None, image_path=None, **kw):
        Label.__init__(self, master=master, **kw)
        if image_path:
            self._add_image(image_path)
        if tool_tip:
            ToolTip(self, text=tool_tip)

    def _add_image(self, path):
        self.image = ImageTk.PhotoImage(Image.open(path))
        self.configure(image=self.image)


class HoverButton(Button):

    def __init__(self, master, image_path=None, keep_pressed=False, **kw):
        Button.__init__(self, master=master, **kw)
        self.defaultBackground = self["background"]
        self.bind("<Enter>", self.on_enter)
        self.bind("<Leave>", self.on_leave)
        if keep_pressed:
            self.bind("<Button-1>", self.on_click)
        if image_path:
            self.image = ImageTk.PhotoImage(Image.open(image_path))
            self.configure(image=self.image)

    def on_click(self, e):
        if self['background'] == self.defaultBackground:
            self['background'] = self['activebackground']
        else:
            self['background'] = self.defaultBackground

    def on_enter(self, e):
        self['background'] = self['activebackground']

    def on_leave(self, e):
        self['background'] = self.defaultBackground


class UserInterface(Frame):

    def __init__(self, master=None, exclude=[], **kw):
        Frame.__init__(self, master, **kw)
        self.background = '#3c3f41'
        self.border_color = '#303030'
        self.active = '#0073ff'
        self.scripts = glob.glob("scripts/*.sh")
        for item in exclude:
            file = 'scripts/' + item + '.sh' 
            if file in self.scripts:
                self.scripts.remove(file)
        self.scripts.sort()
        self.cols = 3
        self.rows = math.ceil(len(self.scripts) / self.cols)
        self._init_ui()

    def _init_ui(self):
        ws = self.master.winfo_screenwidth()
        hs = self.master.winfo_screenheight()
        self.height = 800
        self.width = 800
        x = (ws / 2) - (self.width / 2)
        y = (hs / 2) - (self.height / 2)
        self.master.geometry('%dx%d+%d+%d' % (self.width, self.height, x, y))
        self.master.title("Demo Launcher")
        self.configure(bg=self.background, bd=0, relief=SUNKEN,
                       highlightbackground=self.border_color, highlightthickness=1)

        self.pack(fill=BOTH, expand=True)

        self.columnconfigure(0, weight=1)
        self.rowconfigure(0, weight=0)
        self.rowconfigure(1, weight=1)

        header_frame = Frame(self, bg=self.background, bd=0, relief=SUNKEN,
                             highlightbackground=self.border_color, highlightthickness=1)
        main_frame = Frame(self, bg=self.background, bd=0, relief=SUNKEN,
                           highlightbackground=self.border_color, highlightthickness=1)

        header_frame.grid(row=0, column=0, sticky='news')
        main_frame.grid(row=1, column=0, sticky='news')

        # Header frame
        header_frame.columnconfigure(0, weight=1)

        # Logo
        LabelImage(header_frame, bg=self.background, image_path='resources/logo.png').grid(row=0, column=0,
                                                                                           sticky='news')

        # Main frame
        for i in range(self.rows):
            main_frame.rowconfigure(i, weight=1)

        for j in range(self.cols):
            main_frame.columnconfigure(j, weight=1)

        self.buttons = []
        for i in range(self.rows):
            buttons = []
            for j in range(self.cols):
                idx = i * 3 + j
                if idx >= len(self.scripts):
                    break
                text = self.scripts[idx].replace('.sh', '').replace('scripts/', '').replace('_', ' ')
                buttons.append(HoverButton(main_frame, command=lambda pos=idx: self._click(pos),
                                           text=text, compound='center', font=('Arial', 14, 'bold'),
                                           bd=0.5, fg='#ffffff', bg=self.background, activebackground=self.active))
                buttons[-1].grid(row=i, column=j, sticky='news')
            self.buttons.append(buttons)

    def _click(self, idx):
        subprocess.call(['./' + self.scripts[idx]])


if __name__ == '__main__':
    root = Tk()
    exclude_scripts = ['ros_ip', \
    'transfer_license',\
    'get_mac_address', \
    'send_config_to_ground_station', \
    'send_config_to_rosbot',\
    'remove_mowito']
    UserInterface(root, exclude_scripts)
    root.mainloop()
